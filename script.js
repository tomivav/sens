window.onload = function(){
    if(window.mobilecheck()){
        $('.teaser .title').html("Einfach sicher");
        $('.teaser .text').html("SIXSENSES gibt Dir bescheid, wenn es unerwartete Bewegung erkennt, sodass Du direkt durch die App einen Alarm oder eine Ansprache auslösen kannst.");
        $('.section-4 .title').html("Design vereint</br>mit deutscher Sicherheit");
    }
};

window.mobilecheck = function() {
    var check = false;
    (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

setInterval(function(){
    var bg = $('.bg-image-back');
    var td1 = $('.transition-dots-teaser .transition-dot-1');
    var td2 = $('.transition-dots-teaser .transition-dot-2');
    var td3 = $('.transition-dots-teaser .transition-dot-3');
    var td4 = $('.transition-dots-teaser .transition-dot-4');
    if(bg.hasClass('bg-image-back-1')){
        bg.removeClass('bg-image-back-1');
        td1.removeClass('transition-dot-full');
        bg.addClass('bg-image-back-2');
        td2.addClass('transition-dot-full');
    } else if(bg.hasClass('bg-image-back-2')){
        bg.removeClass('bg-image-back-2');
        td2.removeClass('transition-dot-full');
        bg.addClass('bg-image-back-3');
        td3.addClass('transition-dot-full');
    } else if(bg.hasClass('bg-image-back-3')){
        bg.removeClass('bg-image-back-3');
        td3.removeClass('transition-dot-full');
        bg.addClass('bg-image-back-4');
        td4.addClass('transition-dot-full');
    } else{
        bg.removeClass('bg-image-back-4');
        td4.removeClass('transition-dot-full');
        bg.addClass('bg-image-back-1');
        td1.addClass('transition-dot-full');
    }
}, 5000);

setInterval(function(){
    var tm = $('.testimonial');
    var p1 = $('.person-1');
    var p2 = $('.person-2');
    var p3 = $('.person-3');
    var p4 = $('.person-4');
    var p5 = $('.person-5');
    var p6 = $('.person-6');
    var td1 = $('.transition-dots-testimonials .transition-dot-1');
    var td2 = $('.transition-dots-testimonials .transition-dot-2');
    var td3 = $('.transition-dots-testimonials .transition-dot-3');
    var td4 = $('.transition-dots-testimonials .transition-dot-4');
    var td5 = $('.transition-dots-testimonials .transition-dot-5');
    var td6 = $('.transition-dots-testimonials .transition-dot-6');
    if(p1.hasClass('person-highlighted')){
        p1.removeClass('person-highlighted');
        td1.removeClass('transition-dot-full');
        p2.addClass('person-highlighted');
        tm.fadeOut(250, function(){
            tm.html('“Ich kann von unterwegs ' +
                'den Einbrecher direkt ansprechen und auch Alarme aktivieren. ' +
                'Das ist mal was Neues.”');
            tm.fadeIn(250);
        });
        td2.addClass('transition-dot-full');
    } else if(p2.hasClass('person-highlighted')){
        p2.removeClass('person-highlighted');
        td2.removeClass('transition-dot-full');
        p3.addClass('person-highlighted');
        tm.fadeOut(250, function(){
            tm.html('“Cooles Design. Besser als die üblichen weißen ' +
                'Sicherheitsprodukte aus Plastik.”');
            tm.fadeIn(250);
        });
        td3.addClass('transition-dot-full');
    } else if(p3.hasClass('person-highlighted')){
        p3.removeClass('person-highlighted');
        td3.removeClass('transition-dot-full');
        p4.addClass('person-highlighted');
        tm.fadeOut(250, function(){
            tm.html('“Ideal für meine Großmutter. Damit kann sie weiterhin in ihrem ' +
                'Zuhause bleiben aber im Notfall jederzeit Kontakt mit uns ' +
                'aufnehmen.”');
            tm.fadeIn(250);
        });
        td4.addClass('transition-dot-full');
    } else if(p4.hasClass('person-highlighted')){
        p4.removeClass('person-highlighted');
        td4.removeClass('transition-dot-full');
        p5.addClass('person-highlighted');
        tm.fadeOut(250, function(){
            tm.html('“Ich kann von unterwegs den Einbrecher direkt ansprechen und ' +
                'auch Alarme aktivieren. Das ist mal was Neues.”');
            tm.fadeIn(250);
        });
        td5.addClass('transition-dot-full');
    } else if(p5.hasClass('person-highlighted')){
        p5.removeClass('person-highlighted');
        td5.removeClass('transition-dot-full');
        p6.addClass('person-highlighted');
        tm.fadeOut(250, function(){
            tm.html('“Klasse, dass ich auch meine Nachbarn und Mitbewohner informieren ' +
                'kann, wenn bei uns zuhause was nicht stimmt.”');
            tm.fadeIn(250);
        });
        td6.addClass('transition-dot-full');
    } else{
        p6.removeClass('person-highlighted');
        td6.removeClass('transition-dot-full');
        p1.addClass('person-highlighted');
        tm.fadeOut(250, function(){
            tm.html('“Der Deutsche und lokale Sicherheitsansatz gefällt mir! ' +
                'Nach dem NSA Skandal würde ich so ein Produkt nur von ' +
                'einem deutschen Anbieter kaufen.”');
            tm.fadeIn(250);
        });
        td1.addClass('transition-dot-full');
    }
}, 7000);